ARG python_version=3.8

FROM debian:bullseye-slim as agbcc-builder

RUN apt update && apt install -y \
    gcc \
    make \
    git \
    binutils-arm-none-eabi

RUN mkdir agbcc_install
RUN git clone https://github.com/pret/agbcc.git
RUN cd agbcc && ./build.sh && ./install.sh /agbcc_install


FROM python:${python_version}

COPY --from=agbcc-builder /agbcc_install/tools/agbcc /agbcc

ENV PATH=/agbcc/bin:${PATH}

CMD /bin/bash


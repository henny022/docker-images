.PHONY: login
login:
	docker login registry.gitlab.com

.PHONY: agbcc_python37
agbcc_python37:
	docker build -t registry.gitlab.com/henny022/docker-images/agbcc:python37 -f agbcc-python.DOCKERFILE --build-arg python_version=3.7 .
	docker push registry.gitlab.com/henny022/docker-images/agbcc:python37

.PHONY: agbcc_python38
agbcc_python38:
	docker build -t registry.gitlab.com/henny022/docker-images/agbcc:python38 -f agbcc-python.DOCKERFILE --build-arg python_version=3.8 .
	docker push registry.gitlab.com/henny022/docker-images/agbcc:python38

